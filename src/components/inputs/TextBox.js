import React, { useState, useEffect } from "react";
import { Enum, Utils, Constants } from '../inputs'

const TextBox = (props) => {
    const [error, setError] = useState(props.error ? props.error : {})
    const [fieldValue, setFieldValue] = useState(props.formdata[props.id] ? props.formdata[props.id] : "")

    useEffect(() => {
        setError(props.error ? props.error : {});
        setFieldValue(props.formdata[props.id] ? props.formdata[props.id] : "");
    }, [props])

    const handleChange = (e) => {
        setError(error => ({
            ...error,
            [props.id]: ""
        }))
        let data = e.target.value;

        switch (props.textfor) {
            case Enum.UserName:
                if (Utils.validateRegex(data, Constants.UserNameRegex) == true) {
                    setFieldValue(data);
                    props.formdata[props.id] = data
                }
                break;

            default:
                setFieldValue(data);
                props.formdata[props.id] = data
        }

        if (props.onChange)
            props.onChange(e)
    };

    const onPaste = (e) => {
        if (props.onPaste)
            props.onPaste(e)
    }
    const onBlur = (e) => {
        setError(error => ({
            ...error,
            [props.id]: ""
        }))

        if (props.isPassword && e.target.value.length > 0) {
            if (Utils.validateRegex(e.target.value, Constants.PasswordRegex) == false) {
                setError(error => ({
                    ...error,
                    [props.id]: "Please enter a valid Password"
                }))
            }
        }
        if (props.onBlur)
            props.onBlur(e)
    }

    return (
        <div className={props.inline ? "form-inline" : "form-group"}>
            {props.label ?
                <label className="labels mr-3">
                    {props.label}
                    {props.required ?
                        <span className="redAlert">*</span> :
                        <></>
                    }
                </label> : <></>}
            <div className={props.isGroup ? "input-group" : ""}>
                <input
                    type={props.isPassword ? "password" : "text"}
                    className={"form-control " + (props.className ? props.className : "")}
                    name={props.id}
                    id={props.id}
                    value={fieldValue}
                    onChange={handleChange}
                    onPaste={onPaste}
                    onBlur={onBlur}
                    ref={props.refValue ? props.refValue : null}
                    disabled={props.disabled ? true : false}
                    textfor={props.textfor}
                    placeholder={props.placeholder ? props.placeholder : ""}
                    maxLength={props.maxLength ? props.maxLength : 50}
                    minLength={props.minLength ? props?.minLength : null}
                    autoComplete={props?.autoComplete ? props?.autoComplete : null}
                />
                 {
                    props.helpertext ?
                        <small className="text-muted">
                            {props.helpertext}
                        </small>
                        : <></>
                }
            </div>
            <div className="error" style={{color:'red'}}>{error[props.id]}</div>
        </div>
    );
}

export default TextBox