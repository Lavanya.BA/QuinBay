import './style.css';
import NoImage from '../../assets/NoImage.webp'
import { Carousel } from "react-bootstrap";

const SearchCard = (props) => {
    return (
        <div className='row'>
            <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                <div
                    className="card boder-none cardContainer"
                >
                    <span className='top_image'>paling top</span>
                    <Carousel
                        interval={null}
                        variant="dark"
                    >
                        {props?.data?.images && props?.data?.images.length > 0 && props?.data?.images.map((image, index) => (
                            <Carousel.Item key={index}>
                                <img
                                    className="scrollImage"
                                    src={image ? image : NoImage}
                                    alt={'Image'}
                                />
                            </Carousel.Item>
                        )
                        )
                        }
                    </Carousel>
                    <div
                        className="card-body">
                        <div className='bodyContent'>
                            <div className='name'>
                                <p>{props?.data?.name}</p>
                            </div>
                            <div className='d-flex price'>
                                {props?.data?.price?.priceDisplay}
                            </div>
                            {
                                props?.data?.price?.strikeThroughPriceDisplay &&
                                <>
                                    <div className='d-flex strikePriceContainer'>
                                        <span className='strikePrice'>{props?.data?.price?.strikeThroughPriceDisplay}
                                        </span>
                                        <span className='discount'>{props?.data?.price?.discount}%</span>
                                    </div>

                                </>
                            }
                            {
                                props?.data?.badge?.merchantBadgeUrl && props?.data?.location &&
                                <div className='d-flex'>
                                    {/* <img
                                        src={props?.data?.badge?.merchantBadgeUrl}
                                        style={{ width: '20px', height: '20px', minHeight: '20px', maxHeight: '20px', minWidth: '20px', maxWidth: '20px' }}
                                        alt={'Image'}
                                    /> */}
                                    {props?.data?.location}
                                </div>
                            }
                        </div>
                        <div className='fixedBottom'>
                            <button
                                type="submit"
                                className="btn btn-primary btn-block mt-0"
                                onClick={() => alert('Enter')}
                            >
                                Add to Cart
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default SearchCard;
